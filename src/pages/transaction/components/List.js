import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Table, Modal, Avatar } from 'antd'
import { DropOption } from 'components'
import { Trans, withI18n } from '@lingui/react'
import Link from 'umi/link'
import styles from './List.less'

// const { confirm } = Modal

@withI18n()
class List extends PureComponent {

  // handleMenuClick = (record, e) => {
	// 	const { onDeleteItem, onEditItem, i18n } = this.props
		

  //   if (e.key === '1') {
  //     onEditItem(record)
  //   } else if (e.key === '2') {
  //     confirm({
  //       title: i18n.t`Are you sure delete this record?`,
  //       onOk() {
  //         onDeleteItem(record.id)
  //       },
  //     })
  //   }
  // }

  render() {
		// const { onDeleteItem, onEditItem, i18n, ...tableProps } = this.props
		const { i18n, ...tableProps } = this.props

    const columns = [
      {
        title: <Trans>Sender</Trans>,
        dataIndex: 'sender',
        key: 'sender', 
      },
      {
        title: <Trans>Receiver</Trans>,
        dataIndex: 'receiver',
        key: 'receiver'
			},
			{
        title: <Trans>Created time</Trans>,
        dataIndex: 'created',
        key: 'created',
      },
    ]

    return (
      <Table
        {...tableProps}
        pagination={{
          ...tableProps.pagination,
          showTotal: total => i18n.t`Total ${total} Items`,
        }}
        className={styles.table}
        bordered
        scroll={{ x: 1200 }}
        columns={columns}
        simple
        rowKey={record => record.id}
      />
    )
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  location: PropTypes.object,
}

export default List
