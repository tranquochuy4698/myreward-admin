import modelExtend from 'dva-model-extend'
import { pathMatchRegexp } from 'utils'
import api from 'api'
import { pageModel } from 'utils/model'

const { queryTransactionList } = api

export default modelExtend(pageModel, {
  namespace: 'transaction',
  state: {
		selectedRowKeys: []
	},
  subscriptions: {
		setup({history,dispatch }) {
      history.listen(location => {
        if (pathMatchRegexp('/transaction', location.pathname)) {
          const payload = location.query || { page: 1, pageSize: 10 }
          dispatch({
            type: 'query',
            payload,
          })
        }
      })
    },
  },
  effects: {
    *query({ payload = {} }, { call, put }) {
			const data = yield call(queryTransactionList, payload)			
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.total,
            },
          },
        })
      }
    },
  },
})
