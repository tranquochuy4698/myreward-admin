import { Mock, Constant } from './_utils'

const { ApiPrefix } = Constant

let transcId = 0
const database = Mock.mock({
  'data|50': [
    {
      id() {
        transcId += 1
        return transcId + 10000
      },
      'status|1-3': 1,      
      sender: '@name',
      receiver: '@word',      
      created: '@dateTime',      
    },
  ],
}).data

module.exports = {
  [`GET ${ApiPrefix}/transactions`](req, res) {
    const { query } = req
    let { pageSize, page, ...other } = query
    pageSize = pageSize || 10
    page = page || 1

    let newData = database
    for (let key in other) {
      if ({}.hasOwnProperty.call(other, key)) {
        newData = newData.filter(item => {
          if ({}.hasOwnProperty.call(item, key)) {
            return (
              String(item[key])
                .trim()
                .indexOf(decodeURI(other[key]).trim()) > -1
            )
          }
          return true
        })
      }
    }
    res.status(200).json({			
      data: newData.slice((page - 1) * pageSize, page * pageSize),
      total: newData.length,
    })
  },
}
