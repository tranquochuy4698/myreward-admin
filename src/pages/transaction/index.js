import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { router } from 'utils'
import { connect } from 'dva'
import { Row, Col, Button, Popconfirm } from 'antd'
import { withI18n } from '@lingui/react'
import { Page } from 'components'
import { stringify } from 'qs'
import List from './components/List'


@withI18n()
@connect(({transaction,loading}) => ({transaction,loading}))
class Transaction extends PureComponent {
	
	handleRefresh = newQuery => {
    const { location } = this.props
		const { query, pathname } = location
	
    router.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
	}
	
	get listProps(){
		const { dispatch, transaction, loading } = this.props
		const { list, pagination, selectedRowKeys } = transaction
		return {
			dataSource: list,
			loading: loading.effects['transaction/query'],
			pagination,
			onChange: page => {
        this.handleRefresh({
          page: page.current,
          pageSize: page.pageSize,
        })
      },
      // onDeleteItem: id => {
      //   dispatch({
      //     type: 'user/delete',
      //     payload: id,
      //   }).then(() => {
      //     this.handleRefresh({
      //       page:
      //         list.length === 1 && pagination.current > 1
      //           ? pagination.current - 1
      //           : pagination.current,
      //     })
      //   })
      // },
      // onEditItem(item) {
      //   dispatch({
      //     type: 'user/showModal',
      //     payload: {
      //       modalType: 'update',
      //       currentItem: item,
      //     },
      //   })
      // },
      rowSelection: {
        selectedRowKeys,
        onChange: keys => {					
          dispatch({
            type: 'transaction/updateState',
            payload: {
              selectedRowKeys: keys,
            },
          })
        },
      },
		}
	}
	
	render(){
		return(
		<Page inner>
			<List {...this.listProps} />
		</Page>)
	}
}


Transaction.propTypes = {
	transaction : PropTypes.object,	
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}


export default Transaction